import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:lostlove/models/user.dart';
import 'package:lostlove/pages/activity_feed.dart';
import 'package:lostlove/pages/login_page.dart';
import 'package:lostlove/pages/onboarding.dart';
import 'package:lostlove/pages/profile.dart';
import 'package:lostlove/pages/search.dart';
import 'package:lostlove/pages/timeline.dart';
import 'package:lostlove/pages/upload.dart';
import 'package:shared_preferences/shared_preferences.dart';

final Reference storageRef = FirebaseStorage.instance.ref();
final usersRef = FirebaseFirestore.instance.collection('users');
final postsRef = FirebaseFirestore.instance.collection('posts');
final commentsRef = FirebaseFirestore.instance.collection('comments');
final activityFeedRef = FirebaseFirestore.instance.collection('feed');
final followersRef = FirebaseFirestore.instance.collection('followers');
final followingRef = FirebaseFirestore.instance.collection('following');
final timelineRef = FirebaseFirestore.instance.collection('timeline');
final notificationRef = FirebaseFirestore.instance.collection('notifications');
LostLoveUser currentUser;

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  PageController pageController;
  int pageIndex = 0;

  @override
  void initState() {
    super.initState();
    // Initialize pageController widget
    pageController = PageController();

    // Detects when user signed in or signed out
    /*FirebaseAuth.instance.currentUser().then((currentUser) {
      if (currentUser == null) {
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => LoginPage(),
          ),
        );
      }
    });*/

    isConnected();

    _checkLoginInfo();
  }

  /// Check if device is connected to the internet and display a toast with the result
  isConnected() async {
    print('looking up address');
    try {
      final result = await InternetAddress.lookup('example.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print('connected to internet');
      } else {
        Fluttertoast.showToast(
          msg: 'not connected to internet',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
        );
        print('not connected to internet');
      }
    } on SocketException catch (_) {
      Fluttertoast.showToast(
        msg: 'not connected to internet',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.blue,
        textColor: Colors.white,
      );
      print('not connected');
    }
  }

  /// Check if user data exists in shared preferences
  _checkLoginInfo() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // Try reading data from the isRegistered and isLoggedIn key. If it doesn't exist, return false.
    bool isRegistered = prefs.getBool('isRegistered') ?? false;
    bool isLoggedIn = prefs.getBool('isLoggedIn') ?? false;
    // if user is not registered then open onboarding page
    if (!isRegistered) {
      print('isRegistered is false');
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => Onboarding(),
        ),
      );
    } else {
      print('isRegistered is true');
      // If user is not logged in then open login page
      if (!isLoggedIn) {
        print('isLoggedIn is false');
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => LoginPage(),
          ),
        );
      } else {
        print('User is logged in');
        print("User ID: ${prefs.getString('uid')}");
        print("uname: ${prefs.getString("username")}");

        //create an instance of the current user
        this.setState(() {
          currentUser = LostLoveUser.fromDocument(
            id: prefs.getString('uid'),
            username: prefs.getString('username'),
            phoneNumber: prefs.getString('phoneNumber'),
            photoUrl: prefs.getString('photoUrl'),
            displayName: prefs.getString('displayName'),
            bio: prefs.getString('bio'),
          );
        });
      }
    }
  }

  // dispose the pageController when we are not on the home page and therefore do not need the pageController
  @override
  void dispose() {
    pageController.dispose();
    super.dispose();
  }

  onPageChanged(int pageIndex) {
    setState(() {
      this.pageIndex = pageIndex;
    });
  }

  // responsible for changing the pages on pageView when the bottom navigation bar is clicked by the user
  onTap(int pageIndex) {
    pageController.animateToPage(
      pageIndex,
      duration: Duration(milliseconds: 300),
      curve: Curves.easeInOut,
    );
  }

  Scaffold homeTab() {
    return Scaffold(
      body: PageView(
        children: <Widget>[
          Timeline(currentUser: currentUser),
          Search(),
          Upload(),
          ActivityFeed(user: currentUser),
          Profile(
            profileId: currentUser?.id,
          ),
        ],
        // the controller allows us to be able to switch between pages
        controller: pageController,
        // this function will take the index we're on in the widget list above in order to display the corresponding page
        onPageChanged: onPageChanged,
        // set the pageview to not be scrollable
        physics: NeverScrollableScrollPhysics(),
      ),
      bottomNavigationBar: CupertinoTabBar(
        currentIndex: pageIndex,
        onTap: onTap,
        // set the color of the button clicked on the bottom navigation bar to our theme's primary color
        activeColor: Theme.of(context).primaryColor,
        items: [
          // icon for the timeline page
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
          ),
          // icon for the search page
          BottomNavigationBarItem(
            icon: Icon(Icons.search),
          ),
          // icon for the uploads page
          BottomNavigationBarItem(
            icon: Icon(
              Icons.add_circle,
              size: 35.0,
            ),
          ),
          // icon for the activity page
          BottomNavigationBarItem(
            icon: Icon(Icons.notifications_active),
          ),
          // icon for the profile page
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return homeTab();
  }
}
