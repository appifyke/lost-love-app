import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import "package:flutter/material.dart";
import 'package:image_picker/image_picker.dart';
import 'package:lostlove/main.dart';
import 'package:lostlove/models/user.dart';
import 'package:lostlove/pages/home.dart';
import 'package:lostlove/widgets/header.dart';
import 'package:lostlove/widgets/progress.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid/uuid.dart';
import 'package:image/image.dart' as Im;

class EditProfile extends StatefulWidget {
  final LostLoveUser currentUser;
  final bool isSigningUp;
  final bool
      isBackEnabled; // true when a previous/initial page launches this page

  EditProfile(
      {this.currentUser, this.isSigningUp = false, this.isBackEnabled = false});

  @override
  _EditProfileState createState() =>
      _EditProfileState(isSigningUp: this.isSigningUp);
}

class _EditProfileState extends State<EditProfile> {
  bool isLoading = false;
  bool isSigningUp;
  LostLoveUser user;
  TextEditingController displayNameController = TextEditingController();
  TextEditingController bioController = TextEditingController();
  TextEditingController lostLoveController = TextEditingController();
  TextEditingController relationshipController = TextEditingController();
  TextEditingController passingController = TextEditingController();
  bool _bioValid = true;
  bool _displayNameValid = true;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  var file;
  String postId = Uuid().v4();

  _EditProfileState({this.isSigningUp});

  @override
  void initState() {
    super.initState();
    getUser();
  }

  getUser() async {
    setState(() {
      isLoading = true;
    });
    try {
      DocumentSnapshot doc = await usersRef.doc(widget.currentUser.id).get();
      user = LostLoveUser.fromDocument(
        id: doc.data()['id'],
        username: doc.data()['username'],
        phoneNumber: doc.data()['phoneNumber'],
        photoUrl: doc.data()['photoUrl'],
        displayName: doc.data()['displayName'],
        bio: doc.data()['bio'],
        lostLove: doc.data()['lostlove'],
        relationship: doc.data()['relationship'],
        passing: doc.data()['passing'],
      );
      displayNameController.text = user.displayName;
      bioController.text = user.bio;
      lostLoveController.text = user.lostLove;
      relationshipController.text = user.relationship;
      passingController.text = user.passing;
      setState(() {
        isLoading = false;
      });
    } catch (e) {
      print("Error occurred: $e");
    }
  }

  Column buildDisplayNameField() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(
            top: 12.0,
          ),
          child: Text(
            "Display name",
            style: TextStyle(
              color: Colors.grey,
            ),
          ),
        ),
        TextField(
          controller: displayNameController,
          decoration: InputDecoration(
              hintText: "Update display name",
              errorText: _displayNameValid ? null : "Display name too short"),
        ),
      ],
    );
  }

  Column buildBioField() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(
            top: 12.0,
          ),
          child: Text(
            "Bio",
            style: TextStyle(
              color: Colors.grey,
            ),
          ),
        ),
        TextField(
          controller: bioController,
          decoration: InputDecoration(
              hintText: "Update bio",
              errorText: _bioValid ? null : "Bio is too long"),
        ),
      ],
    );
  }

  Column buildLostLoveField() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(
            top: 12.0,
          ),
          child: Text(
            "What's the name of the loved one you lost?",
            style: TextStyle(
              color: Colors.grey,
            ),
          ),
        ),
        TextField(
          controller: lostLoveController,
          decoration: InputDecoration(hintText: "Enter his/her name"),
        ),
      ],
    );
  }

  Column buildRelationshipField() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(
            top: 12.0,
          ),
          child: Text(
            "What was your relationship?",
            style: TextStyle(
              color: Colors.grey,
            ),
          ),
        ),
        TextField(
          controller: relationshipController,
          decoration: InputDecoration(hintText: "Enter relationship"),
        ),
      ],
    );
  }

  Column buildPassingField() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(
            top: 12.0,
          ),
          child: Text(
            "How did he/she pass away?",
            style: TextStyle(
              color: Colors.grey,
            ),
          ),
        ),
        TextField(
          controller: passingController,
          decoration: InputDecoration(hintText: "Take your time :-)"),
        ),
      ],
    );
  }

  updateProfileData() {
    setState(() {
      displayNameController.text.trim().length < 3 ||
              displayNameController.text.isEmpty
          ? _displayNameValid = false
          : _displayNameValid = true;
      bioController.text.trim().length > 100
          ? _bioValid = false
          : _bioValid = true;
    });
    if (_displayNameValid && _bioValid) {
      usersRef.doc(widget.currentUser.id).update({
        "displayName": displayNameController.text,
        "bio": bioController.text,
        "lostLove": lostLoveController.text,
        "relationship": relationshipController.text,
        "passing": passingController.text,
      });
      SnackBar snackbar = SnackBar(
        content: Text("Profile updated!"),
      );
      _scaffoldKey.currentState.showSnackBar(snackbar);

      if (isSigningUp) {
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => TheApp(),
          ),
        );
      }
    }
    if (this.file != null) {
      handleSubmit();
    }
  }

  back() {
    if (widget.isBackEnabled) {
      Navigator.pop(context);
    } else {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => TheApp(),
        ),
      );
    }
  }

  handleChooseFromGallery() async {
    final picker = ImagePicker();
    final PickedFile pickedFile =
        await picker.getImage(source: ImageSource.gallery);
    final File file = File(pickedFile.path);

    setState(() {
      this.file = file;
    });

    print("image was picked");
    print("image ${this.file}");
  }

  handleSubmit() async {
    print("handling profile pic");
    await compressImage();
    String mediaUrl = await uploadImage(file);
    addProfilePicInFirestore(
      mediaUrl: mediaUrl,
    );
    setState(() {
      file = null;
      // create a new postId for the next post to avoid overwriting the
      // previous post
      postId = Uuid().v4();
    });
  }

  compressImage() async {
    print("compressing image");
    final tempDir = await getTemporaryDirectory();
    final path = tempDir.path;
    Im.Image imageFile = Im.decodeImage(file.readAsBytesSync());
    final compressedImageFile = File('$path/img_$postId.jpg')
      ..writeAsBytesSync(Im.encodeJpg(imageFile, quality: 85));
    setState(() {
      file = compressedImageFile;
    });
  }

  Future<String> uploadImage(imageFile) async {
    print("uploading profile pic");
    UploadTask uploadTask =
        storageRef.child("post_$postId.jpg").putFile(imageFile);
    TaskSnapshot storageSnap = await uploadTask;
    String downloadUrl = await storageSnap.ref.getDownloadURL();
    return downloadUrl;
  }

  addProfilePicInFirestore({String mediaUrl}) {
    print("adding to firestore");
    usersRef.doc(widget.currentUser.id).update({
      "photoUrl": mediaUrl,
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: header(
        context,
        titleText: "Edit profile",
        executeFunction: true,
        yourFunction: back,
      ),
      body: isLoading
          ? circularProgress(context)
          : ListView(
              children: <Widget>[
                Container(
                  child: Column(children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 16.0, bottom: 8.0),
                      child: GestureDetector(
                        onTap: handleChooseFromGallery,
                        child: CircleAvatar(
                          radius: 50.0,
                          backgroundImage: this.file == null
                              ? user.photoUrl.isEmpty || user.photoUrl == 'url'
                                  ? null
                                  : CachedNetworkImageProvider(user.photoUrl)
                              : FileImage(this.file),
                          child: this.file == null
                              ? user.photoUrl.isEmpty || user.photoUrl == 'url'
                                  ? Text(
                                      'Click to set photo',
                                      textAlign: TextAlign.center,
                                    )
                                  : null
                              : null,
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(16.0),
                      child: Column(
                        children: <Widget>[
                          buildDisplayNameField(),
                          buildBioField(),
                          buildLostLoveField(),
                          buildRelationshipField(),
                          buildPassingField(),
                        ],
                      ),
                    ),
                    RaisedButton(
                      onPressed: updateProfileData,
                      child: Text(isSigningUp ? "Continue" : "Update"),
                    ),
                  ]),
                ),
              ],
            ),
    );
  }
}
