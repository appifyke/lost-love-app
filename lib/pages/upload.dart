import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_picker/image_picker.dart';
import 'package:lostlove/models/user.dart';
import 'package:lostlove/widgets/header.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:lostlove/widgets/progress.dart';
import 'package:path_provider/path_provider.dart';
import 'package:image/image.dart' as Im;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid/uuid.dart';
import 'package:lostlove/pages/home.dart';
import 'package:geocoder/geocoder.dart';

class Upload extends StatefulWidget {
  @override
  _UploadState createState() => _UploadState();
}

class _UploadState extends State<Upload>
    with AutomaticKeepAliveClientMixin<Upload> {
  final databaseReference = FirebaseFirestore.instance;
  final postController = TextEditingController();
  TextEditingController locationController = TextEditingController();
  TextEditingController captionController = TextEditingController();
  var file;
  bool isUploading = false;
  String postId = Uuid().v4();
  //TODO stop using shared preferences and instead pass user data from class to class
  SharedPreferences prefs;

  @override
  void initState() {
    super.initState();
    _initializePrefs();
  }

  _initializePrefs() async {
    prefs = await SharedPreferences.getInstance();
  }

  compressImage() async {
    final tempDir = await getTemporaryDirectory();
    final path = tempDir.path;
    Im.Image imageFile = Im.decodeImage(file.readAsBytesSync());
    final compressedImageFile = File('$path/img_$postId.jpg')
      ..writeAsBytesSync(Im.encodeJpg(imageFile, quality: 85));
    setState(() {
      file = compressedImageFile;
    });
  }

  Future<String> uploadImage(imageFile) async {
    UploadTask uploadTask =
        storageRef.child("post_$postId.jpg").putFile(imageFile);
    TaskSnapshot storageSnap = await uploadTask;
    String downloadUrl = await storageSnap.ref.getDownloadURL();
    return downloadUrl;
  }

  createPostInFirestore({
    String mediaUrl,
    String location,
    String description,
  }) {
    postsRef
        .doc(prefs.getString('uid'))
        .collection('userPosts')
        .doc(postId)
        .set({
      "postId": postId,
      "ownerId": prefs.getString('uid'),
      "username": prefs.getString('username'),
      "mediaUrl": mediaUrl,
      "description": description,
      "location": location,
      "timestamp": DateTime.now(),
      "likes": {},
    });

    onCreatePost(
      mediaUrl: mediaUrl,
      location: location,
      description: description,
      pid: postId,
    );
  }

  /// When a post is created, add it to the timeline of
  /// each follower of the post owner
  onCreatePost({
    String mediaUrl,
    String location,
    String description,
    String pid,
  }) async {
    // Get the current user's followers from the followersRef
    QuerySnapshot followersSnapshot = await followersRef
        .doc(currentUser.id)
        .collection("userFollowers")
        .get();
    // Add the newly created post to each followers' timelines
    followersSnapshot.docs.forEach((doc) {
      if (doc.exists) {
        timelineRef.doc(doc.id).collection("timelinePosts").doc(pid).set({
          "postId": pid,
          "ownerId": prefs.getString('uid'),
          "username": prefs.getString('username'),
          "mediaUrl": mediaUrl,
          "description": description,
          "location": location,
          "timestamp": DateTime.now(),
          "likes": {},
        });
      }
    });
  }

  handleSubmit() async {
    setState(() {
      isUploading = true;
    });
    await compressImage();
    String mediaUrl = await uploadImage(file);
    createPostInFirestore(
      mediaUrl: mediaUrl,
      location: locationController.text,
      description: captionController.text,
    );
    captionController.clear();
    locationController.clear();
    setState(() {
      file = null;
      isUploading = false;
      // create a new postId for the next post to avoid overwriting the
      // previous post
      postId = Uuid().v4();
    });
  }

  handleTakePhoto() async {
    final picker = ImagePicker();
    final PickedFile pickedFile =
        await picker.getImage(source: ImageSource.camera);
    final File file = File(pickedFile.path);

    setState(() {
      this.file = file;
    });
  }

  handleChooseFromGallery() async {
    final picker = ImagePicker();
    final PickedFile pickedFile =
        await picker.getImage(source: ImageSource.gallery);
    final File file = File(pickedFile.path);

    setState(() {
      this.file = file;
    });
  }

  selectImage(parentContext) {
    return showDialog(
      context: parentContext,
      builder: (context) {
        return SimpleDialog(
          title: Text("Create post"),
          children: <Widget>[
            SimpleDialogOption(
              child: Text("Photo with camera"),
              onPressed: () {
                // removes the dialog from screen
                Navigator.pop(context);
                handleTakePhoto();
              },
            ),
            SimpleDialogOption(
              child: Text("Image from gallery"),
              onPressed: () {
                Navigator.pop(context);
                handleChooseFromGallery();
              },
            ),
            SimpleDialogOption(
              child: Text("Cancel"),
              onPressed: () => Navigator.pop(context),
            ),
          ],
        );
      },
    );
  }

  createPostInterface() {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: TextField(
            controller: postController,
            decoration: InputDecoration(
              hintText: 'What would you like to share?',
              border: InputBorder.none,
            ),
          ),
        ),
        SizedBox(
          height: 10.0,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            FlatButton(
              child: Text(
                '+ Add photos or videos',
                style: TextStyle(color: Colors.blue, fontSize: 15.0),
              ),
              onPressed: () => selectImage(context),
            ),
          ],
        )
      ],
    );
  }

  buildUploadForm() {
    return Scaffold(
      body: ListView(
        children: <Widget>[
          // display a linear progress bar when isUploading is set to true
          isUploading ? linearProgress(context) : Text(""),
          Container(
            height: 220.0,
            // set width to 80% of available width on screen
            width: MediaQuery.of(context).size.width * 0.8,
            child: Center(
              child: AspectRatio(
                aspectRatio: 16 / 9,
                child: Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: FileImage(file),
                    ),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 10.0),
          ),
          ListTile(
            leading: CircleAvatar(
              backgroundImage:
                  CachedNetworkImageProvider(prefs.getString('photoUrl')),
            ),
            title: Container(
              width: 250.0,
              child: TextField(
                controller: captionController,
                decoration: InputDecoration(
                  hintText: "Write a caption",
                  border: InputBorder.none,
                ),
              ),
            ),
          ),
          Divider(),
          ListTile(
            leading: Icon(
              Icons.pin_drop,
              color: Colors.blue,
              size: 35.0,
            ),
            title: Container(
              width: 250.0,
              child: TextField(
                controller: locationController,
                decoration: InputDecoration(
                  hintText: "Where was this photo taken?",
                  border: InputBorder.none,
                ),
              ),
            ),
          ),
          Container(
            width: 200.0,
            height: 100.0,
            alignment: Alignment.center,
            child: RaisedButton.icon(
              onPressed: getUserLocation,
              icon: Icon(
                Icons.my_location,
                color: Colors.white,
              ),
              label: Text(
                "Use current location",
                style: TextStyle(color: Colors.white),
              ),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0),
              ),
              color: Colors.blue,
            ),
          )
        ],
      ),
    );
  }

  getUserLocation() async {
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);

    // this will get the coordinates from the lat-long using Geocoder Coordinates
    final coordinates = Coordinates(position.latitude, position.longitude);

    var addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var firstAddress = addresses.first;

    String userLocation =
        "${firstAddress.locality}, ${firstAddress.countryName}";
    print("User location is: $userLocation");
    locationController.text = userLocation;
  }

  clearImage() {
    setState(() {
      file = null;
    });
  }

  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      appBar: file == null
          ? header(context, titleText: "Create post")
          : AppBar(
              backgroundColor: Theme.of(context).accentColor,
              leading: IconButton(
                  icon: Icon(Icons.arrow_back, color: Colors.black),
                  onPressed: clearImage),
              title: Text(
                "Create Post",
                style: TextStyle(color: Colors.black, fontSize: 18.0),
              ),
            ),
      body: file == null ? createPostInterface() : buildUploadForm(),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.blue,
        child: Icon(
          Icons.send,
        ),
        onPressed: isUploading ? null : () => handleSubmit(),
      ),
    );
  }
}
