import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:emoji_picker_flutter/emoji_picker_flutter.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_picker/image_picker.dart';
import 'package:lostlove/models/user.dart';
import 'package:lostlove/pages/home.dart';
import 'package:lostlove/pages/login_page.dart';
import 'package:lostlove/widgets/header.dart';
import 'package:lostlove/widgets/post.dart';
import 'package:lostlove/widgets/progress.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid/uuid.dart';
import 'package:image/image.dart' as Im;
import 'package:flutter/services.dart';

class Timeline extends StatefulWidget {
  final LostLoveUser currentUser;

  Timeline({this.currentUser});

  @override
  _TimelineState createState() => _TimelineState();
}

class _TimelineState extends State<Timeline>
    with AutomaticKeepAliveClientMixin<Timeline> {
  final databaseReference = FirebaseFirestore.instance;

  final postController = TextEditingController();
  TextEditingController locationController = TextEditingController();
  TextEditingController captionController = TextEditingController();
  TextEditingController controller = TextEditingController();

  var file;

  String postId = Uuid().v4();

  List<Post> posts;

  FocusNode focusNode = FocusNode();

  bool isTextEmpty = true;
  bool shouldPop = true;
  bool showEmojiPicker = false;
  bool isUploading = false;

  //TODO stop using shared preferences and instead pass user data from class to class
  SharedPreferences prefs;

  @override
  void initState() {
    //createUser();
    //updateUser();
    getTimeline();

    //deleteUser();

    focusNode.addListener(() {
      if (focusNode.hasFocus) {
        setState(() {
          showEmojiPicker = false;
        });
      }
    });

    super.initState();
    _initializePrefs();
  }

  _initializePrefs() async {
    prefs = await SharedPreferences.getInstance();
  }

  Future<void> getTimeline() async {
    setState(() {
      this.posts = null;
    });

    QuerySnapshot snapshot = await timelineRef
        .doc(widget.currentUser?.id)
        .collection("timelinePosts")
        .orderBy("timestamp", descending: true)
        .get();

    List<Post> posts =
        snapshot.docs.map((doc) => Post.fromDocument(doc)).toList();

    if (mounted) {
      setState(() {
        this.posts = posts;
      });
    }
    print("Getting timeline posts: $posts");
    print("who is this: ${widget.currentUser?.id}");
  }

  createUser() {
    usersRef.doc('asknkjshfa').set({
      'username': 'brian',
      'phonenumber': '+254728499458',
      'password': '111',
      'bio': null,
      'isAdmin': false,
      'datetime': DateTime.now(),
    });
  }

  updateUser() async {
    final doc = await usersRef.doc('n56Ow6vA8Hidy4maYO22').get();
    if (doc.exists) {
      doc.reference.update({
        'username': 'amadeus',
        'phonenumber': '+254728499458',
        'password': '111',
        'bio': null,
        'isAdmin': false,
        'datetime': DateTime.now(),
      });
    }
  }

  deleteUser() async {
    final doc = await usersRef.doc('n56Ow6vA8Hidy4maYO22').get();
    if (doc.exists) {
      doc.reference.delete();
    }
  }

  buildTimeline() {
    if (posts == null) {
      return circularProgress;
    } else if (posts.isEmpty) {
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              child: Text(
                "No posts",
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
              padding: EdgeInsets.all(10),
            ),
            Padding(
              padding: EdgeInsets.all(10),
              child: FlatButton(
                color: Theme.of(context).primaryColor,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30),
                ),
                child: Text(
                  "Refresh",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                onPressed: () => getTimeline(),
              ),
            )
          ],
        ),
      );
    } else {
      return Column(
        children: [
          Expanded(child: ListView(children: posts)),
          Align(
            heightFactor: 1.0,
            alignment: Alignment.bottomCenter,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width - 60,
                      child: Card(
                        margin: EdgeInsets.only(left: 3, right: 3, bottom: 8),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0)),
                        child: TextFormField(
                          inputFormatters: [
                            LengthLimitingTextInputFormatter(1000),
                          ],
                          controller: controller,
                          onChanged: (string) {
                            setState(() {
                              string.isNotEmpty
                                  ? isTextEmpty = false
                                  : isTextEmpty = true;
                              if (string.length >= 1000) {
                                Fluttertoast.showToast(
                                  msg: 'Character limit reached',
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.BOTTOM,
                                  timeInSecForIosWeb: 1,
                                  backgroundColor: Colors.blue,
                                  textColor: Colors.white,
                                );
                              }
                            });
                          },
                          focusNode: focusNode,
                          textAlignVertical: TextAlignVertical.center,
                          keyboardType: TextInputType.multiline,
                          maxLines: 5,
                          minLines: 1,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: "Share your thoughts",
                              prefixIcon: IconButton(
                                icon: Icon(Icons.insert_emoticon),
                                onPressed: () {
                                  focusNode.unfocus();
                                  focusNode.canRequestFocus = false;
                                  setState(() {
                                    showEmojiPicker = !showEmojiPicker;
                                  });
                                },
                              ),
                              suffixIcon: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  IconButton(
                                    icon: Icon(Icons.attach_file),
                                    onPressed: handleChooseFromGallery,
                                  ),
                                  IconButton(
                                    icon: Icon(Icons.camera_alt),
                                    onPressed: handleTakePhoto,
                                  ),
                                ],
                              ),
                              contentPadding: EdgeInsets.all(7)),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        bottom: 8.0,
                        right: 2,
                        left: 2,
                      ),
                      child: CircleAvatar(
                        radius: 25,
                        child: isTextEmpty
                            ? IconButton(
                                icon: Icon(
                                  Icons.mic,
                                  color: Colors.white,
                                ),
                                onPressed: () {},
                              )
                            : IconButton(
                                icon: Icon(
                                  Icons.send,
                                  color: Colors.white,
                                ),
                                onPressed: () => postText(controller.text),
                              ),
                      ),
                    ),
                  ],
                ),
                showEmojiPicker ? emojiSelect() : Container(),
              ],
            ),
          )
        ],
      );
    }
  }

  Widget emojiSelect() {
    return Offstage(
      offstage: !showEmojiPicker,
      child: SizedBox(
        height: 250,
        child: EmojiPicker(
          onEmojiSelected: (category, emoji) {
            controller
              ..text += emoji.emoji
              ..selection = TextSelection.fromPosition(
                  TextPosition(offset: controller.text.length));
          },
          config: const Config(
              columns: 7,
              emojiSizeMax: 30.0,
              verticalSpacing: 0,
              horizontalSpacing: 0,
              initCategory: Category.RECENT,
              bgColor: Color(0xFFF2F2F2),
              indicatorColor: Colors.blue,
              iconColor: Colors.grey,
              iconColorSelected: Colors.blue,
              progressIndicatorColor: Colors.blue,
              showRecentsTab: true,
              recentsLimit: 28,
              noRecentsText: 'No Recents',
              noRecentsStyle: TextStyle(fontSize: 20, color: Colors.black26),
              categoryIcons: CategoryIcons(),
              buttonMode: ButtonMode.MATERIAL),
        ),
      ),
    );
  }

  handleTakePhoto() async {
    final picker = ImagePicker();
    final PickedFile pickedFile =
        await picker.getImage(source: ImageSource.camera);
    final File file = File(pickedFile.path);

    setState(() {
      this.file = file;
    });
  }

  handleChooseFromGallery() async {
    final picker = ImagePicker();
    final PickedFile pickedFile =
        await picker.getImage(source: ImageSource.gallery);
    final File file = File(pickedFile.path);

    setState(() {
      this.file = file;
    });
  }

  handleSubmit() async {
    setState(() {
      isUploading = true;
    });
    await compressImage();
    String mediaUrl = await uploadImage(file);
    createPostInFirestore(
      mediaUrl: mediaUrl,
      location: locationController.text,
      description: captionController.text,
    );
    captionController.clear();
    locationController.clear();
    setState(() {
      file = null;
      isUploading = false;
      // create a new postId for the next post to avoid overwriting the
      // previous post
      postId = Uuid().v4();
    });
  }

  postText(String textPost) async {
    createPostInFirestore(
      mediaUrl: "",
      location: "",
      description: textPost,
    );

    controller.clear();

    setState(() {
      // create a new postId for the next post to avoid overwriting the
      // previous post
      postId = Uuid().v4();
    });
  }

  compressImage() async {
    final tempDir = await getTemporaryDirectory();
    final path = tempDir.path;
    Im.Image imageFile = Im.decodeImage(file.readAsBytesSync());
    final compressedImageFile = File('$path/img_$postId.jpg')
      ..writeAsBytesSync(Im.encodeJpg(imageFile, quality: 85));
    setState(() {
      file = compressedImageFile;
    });
  }

  Future<String> uploadImage(imageFile) async {
    UploadTask uploadTask =
        storageRef.child("post_$postId.jpg").putFile(imageFile);
    TaskSnapshot storageSnap = await uploadTask;
    String downloadUrl = await storageSnap.ref.getDownloadURL();
    return downloadUrl;
  }

  createPostInFirestore({
    String mediaUrl,
    String location,
    String description,
  }) {
    postsRef
        .doc(prefs.getString('uid'))
        .collection('userPosts')
        .doc(postId)
        .set({
      "postId": postId,
      "ownerId": prefs.getString('uid'),
      "username": prefs.getString('username'),
      "mediaUrl": mediaUrl,
      "description": description,
      "location": location,
      "timestamp": DateTime.now(),
      "likes": {},
    });

    onCreatePost(
      mediaUrl: mediaUrl,
      location: location,
      description: description,
      pid: postId,
    );
  }

  /// When a post is created, add it to the timeline of
  /// each follower of the post owner
  onCreatePost({
    String mediaUrl,
    String location,
    String description,
    String pid,
  }) async {
    // Get the current user's followers from the followersRef
    QuerySnapshot followersSnapshot = await followersRef
        .doc(currentUser.id)
        .collection("userFollowers")
        .get();
    // Add the newly created post to each followers' timelines
    followersSnapshot.docs.forEach((doc) {
      if (doc.exists) {
        timelineRef.doc(doc.id).collection("timelinePosts").doc(pid).set({
          "postId": pid,
          "ownerId": prefs.getString('uid'),
          "username": prefs.getString('username'),
          "mediaUrl": mediaUrl,
          "description": description,
          "location": location,
          "timestamp": DateTime.now(),
          "likes": {},
        });
      }
    });
  }

  clearImage() {
    setState(() {
      file = null;
    });
  }

  getUserLocation() async {
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);

    // this will get the coordinates from the lat-long using Geocoder Coordinates
    final coordinates = Coordinates(position.latitude, position.longitude);

    var addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var firstAddress = addresses.first;

    String userLocation =
        "${firstAddress.locality}, ${firstAddress.countryName}";
    print("User location is: $userLocation");
    locationController.text = userLocation;
  }

  buildUploadForm() {
    return Scaffold(
      body: ListView(
        children: <Widget>[
          // display a linear progress bar when isUploading is set to true
          isUploading ? linearProgress(context) : Text(""),
          Container(
            height: 220.0,
            // set width to 80% of available width on screen
            width: MediaQuery.of(context).size.width * 0.8,
            child: Center(
              child: AspectRatio(
                aspectRatio: 16 / 9,
                child: Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: FileImage(file),
                    ),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 10.0),
          ),
          ListTile(
            leading: CircleAvatar(
              backgroundImage:
                  CachedNetworkImageProvider(prefs.getString('photoUrl')),
            ),
            title: Container(
              width: 250.0,
              child: TextField(
                controller: captionController,
                decoration: InputDecoration(
                  hintText: "Write a caption",
                  border: InputBorder.none,
                ),
              ),
            ),
          ),
          Divider(),
          ListTile(
            leading: Icon(
              Icons.pin_drop,
              color: Colors.blue,
              size: 35.0,
            ),
            title: Container(
              width: 250.0,
              child: TextField(
                controller: locationController,
                decoration: InputDecoration(
                  hintText: "Where was this photo taken?",
                  border: InputBorder.none,
                ),
              ),
            ),
          ),
          Container(
            width: 200.0,
            height: 100.0,
            alignment: Alignment.center,
            child: RaisedButton.icon(
              onPressed: getUserLocation,
              icon: Icon(
                Icons.my_location,
                color: Colors.white,
              ),
              label: Text(
                "Use current location",
                style: TextStyle(color: Colors.white),
              ),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0),
              ),
              color: Colors.blue,
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(context) {
    super.build(context);

    /// Override back button so that it closes the app when pressed
    return WillPopScope(
      onWillPop: () async {
        //SystemNavigator.pop();
        SystemChannels.platform.invokeMethod("SystemNavigator.pop");
        return shouldPop;
      },
      child: Scaffold(
        appBar: file == null
            ? header(context, isAppTitle: true)
            : AppBar(
                backgroundColor: Theme.of(context).accentColor,
                leading: IconButton(
                    icon: Icon(Icons.arrow_back, color: Colors.black),
                    onPressed: clearImage),
                title: Text(
                  "Create Post",
                  style: TextStyle(color: Colors.black, fontSize: 18.0),
                ),
              ),
        body: file == null
            ? Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: WillPopScope(
                  onWillPop: () {
                    if (showEmojiPicker) {
                      setState(() {
                        showEmojiPicker = false;
                      });
                    } else {
                      Navigator.pop(context);
                    }
                    return Future.value(false);
                  },
                  child: Stack(
                    children: [
                      RefreshIndicator(
                        onRefresh: getTimeline,
                        child: buildTimeline(),
                      ),
                    ],
                  ),
                ),
              )
            : buildUploadForm(),
        floatingActionButton: file == null
            ? null
            : FloatingActionButton(
                backgroundColor: Colors.blue,
                child: Icon(
                  Icons.send,
                ),
                onPressed: isUploading ? null : () => handleSubmit(),
              ),
      ),
    );
  }

  bool get wantKeepAlive => true;
}
