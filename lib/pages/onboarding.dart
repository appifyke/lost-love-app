import 'package:flutter/material.dart';
import 'package:lostlove/pages/login_page.dart';
import 'package:transformer_page_view/transformer_page_view.dart';
import 'package:flutter/cupertino.dart';

class Onboarding extends StatefulWidget {
  @override
  _OnboardingState createState() => _OnboardingState();
}

class _OnboardingState extends State<Onboarding> {
  Color backIconColor = Colors.grey;
  int _slideIndex;

  final List<String> images = [
    "assets/images/welcome.jpg",
    "assets/images/signup.jpg",
    "assets/images/posts.jpg",
    "assets/images/personal.jpg",
    "assets/images/connect.jpg"
  ];

  final List<String> text0 = [
    "Welcome to Lost Love",
    "Sign up",
    "Post",
    "Personal",
    "Connect"
  ];

  final List<String> text1 = [
    "The app that helps you to come to terms with the loss of a loved one through connecting with others who went through the same experience.",
    "Simply sign up using a username, password and phone number then create your profile by filling in a few details about you.",
    "Post whatever is on your mind as well as view other people's posts. We encourage you to post something positive too so as to inspire and encourage others.",
    "Upload memories of your loved one in the archive, set reminders, like birthdays and anniversaries, as well as put down your thoughts and feelings in a journal. These are secure and visible only to you.",
    "Friend others who have had the same experience as you so that you can chat, sharing your experiences and supporting each other. You are not alone."
  ];

  @override
  void initState() {
    super.initState();
    _slideIndex = 0;
  }

  void nextSlide() {
    setState(() {
      // increase slideIndex by 1 when the forward iconButton is clicked and also
      // change the color of the back iconButton to blue
      _slideIndex += 1;
      if (backIconColor == Colors.grey) {
        backIconColor = Colors.blue;
      }
      print('Slide index: $_slideIndex');
      print('List length: ${text0.length - 1}');
    });
  }

  void previousSlide() {
    setState(() {
      // decrease slideIndex by 1 when the back iconButton is clicked and also
      // change the color of the back iconButton to grey once slideIndex hits 0
      _slideIndex -= 1;
      if (_slideIndex == 0) {
        backIconColor = Colors.grey;
      }
      print('Slide index: $_slideIndex');
      print('List length: ${text0.length - 1}');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: <Widget>[
          Expanded(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Image.asset(images[_slideIndex]),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          text0[_slideIndex],
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 17.0,
                            fontFamily: 'Open Sans',
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          text1[_slideIndex],
                          style: TextStyle(
                            fontSize: 17.0,
                            fontFamily: 'Open Sans',
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(
                    8.0,
                    8.0,
                    35.0,
                    8.0,
                  ),
                  child: IconButton(
                    icon: Icon(
                      Icons.arrow_back,
                      color: backIconColor,
                    ),
                    onPressed: () {
                      if (_slideIndex != 0) {
                        previousSlide();
                      } else {
                        print('Not going back');
                      }
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(
                    35.0,
                    8.0,
                    8.0,
                    8.0,
                  ),
                  child: IconButton(
                    icon: Icon(
                      Icons.arrow_forward,
                      color: Colors.blue,
                    ),
                    onPressed: () {
                      if (_slideIndex != text0.length - 1) {
                        nextSlide();
                      } else {
                        print('Open login');
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                            builder: (context) => LoginPage(),
                          ),
                        );
                      }
                    },
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
