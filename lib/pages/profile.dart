import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:lostlove/models/user.dart';
import 'package:lostlove/pages/archived_posts.dart';
import 'package:lostlove/pages/edit_profile.dart';
import 'package:lostlove/pages/home.dart';
import 'package:lostlove/pages/timeline.dart';
import 'package:lostlove/widgets/header.dart';
import 'package:lostlove/widgets/post.dart';
import 'package:lostlove/widgets/post_tile.dart';
import 'package:lostlove/widgets/progress.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:timeago/timeago.dart' as timeago;

import 'login_page.dart';

class Profile extends StatefulWidget {
  final String profileId;

  Profile({this.profileId});

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  FirebaseAuth _auth = FirebaseAuth.instance;
  SharedPreferences prefs;
  final LostLoveUser currentUserStoredInPrefs = currentUser;
  bool isLoading = false;
  int postCount = 0;
  int followersCount = 0;
  int followingCount = 0;
  List<Post> posts = [];
  String postOrientation = "grid";
  bool isFollowing = false;
  Timestamp timestamp;

  @override
  void initState() {
    super.initState();
    _initializePrefs();

    getProfilePosts();
  }

  getProfilePosts() async {
    setState(() {
      isLoading = true;
    });
    QuerySnapshot snapshot = await postsRef
        .doc(widget.profileId)
        .collection("userPosts")
        .where("isArchived", isEqualTo: false)
        .orderBy("timestamp", descending: true)
        .get();
    setState(() {
      isLoading = false;
      postCount = snapshot.docs.length;
      posts = snapshot.docs.map((doc) => Post.fromDocument(doc)).toList();
    });
  }

  _initializePrefs() async {
    prefs = await SharedPreferences.getInstance();
    print("My UID: ${prefs.getString('uid')}");
    print("The current user: ${widget.profileId}");
  }

  logOutUser() async {
    await _auth.signOut();
    // Access shared preferences and set isLoggedIn to false indicating that the user has logged out
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('isLoggedIn', false);
    print('User logged in is: ${prefs.getBool('isLoggedIn')}');
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (context) => LoginPage(),
      ),
    );
  }

  editProfile() {
    // open the edit user profile page
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => EditProfile(
            currentUser: currentUserStoredInPrefs, isBackEnabled: true),
      ),
    );
  }

  buildButton({String text, Function function}) {
    return FlatButton(
      onPressed: function,
      child: Container(
        width: 100.0,
        height: 27.0,
        child: Text(
          text,
          style: TextStyle(
              color: isFollowing ? Colors.black : Colors.white,
              fontWeight: FontWeight.bold),
        ),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: isFollowing ? Colors.white : Colors.blue,
          border: Border.all(color: isFollowing ? Colors.grey : Colors.blue),
          borderRadius: BorderRadius.circular(30.0),
        ),
      ),
    );
  }

  buildProfileButton() {
    // if viewing our own profile then show the edit profile button
    bool isProfileOwner = currentUserStoredInPrefs.id == widget.profileId;
    if (isProfileOwner) {
      return buildButton(
        text: "Edit profile",
        function: editProfile,
      );
    }
  }

  handleUnfollowUser() {
    setState(() {
      isFollowing = false;
    });
    // Remove the user from another user's followers
    // collection
    followersRef
        .doc(widget.profileId)
        .collection("userFollowers")
        .doc(currentUser.id)
        .get()
        .then((doc) {
      if (doc.exists) {
        doc.reference.delete();
      }
    });
    // Remove an unfollowed user from the current user's
    // following collection
    followingRef
        .doc(currentUser.id)
        .collection("userFollowing")
        .doc(widget.profileId)
        .get()
        .then((doc) {
      if (doc.exists) {
        doc.reference.delete();
      }
    });
    // Delete the activity feed item notifying the user of a
    // new follower
    activityFeedRef
        .doc(widget.profileId)
        .collection("feedItems")
        .doc(currentUser.id)
        .get()
        .then((doc) {
      if (doc.exists) {
        doc.reference.delete();
      }
    });
    removeFromTimeline();
  }

  handleFollowUser() {
    setState(() {
      isFollowing = true;
    });
    // Make the user a follower of another user and update
    // the followed user's followers collection
    followersRef
        .doc(widget.profileId)
        .collection("userFollowers")
        .doc(currentUser.id)
        .set({});
    // Add the followed user into the current user's
    // following collection
    followingRef
        .doc(currentUser.id)
        .collection("userFollowing")
        .doc(widget.profileId)
        .set({});
    // Add activity feed item for the followed user to be
    // notified of new follower
    activityFeedRef
        .doc(widget.profileId)
        .collection("feedItems")
        .doc(currentUser.id)
        .set({
      "type": "follow",
      "ownerId": widget.profileId,
      "username": currentUser.username,
      "userId": currentUser.id,
      "userProfileImg": currentUser.photoUrl,
      "timestamp": DateTime.now(),
    });
    addToTimeline();
  }

  addToTimeline() async {
    // Get the posts of the user we have just followed
    QuerySnapshot postsSnapshot = await postsRef
        .doc(widget.profileId)
        .collection("userPosts")
        .orderBy("timestamp", descending: true)
        .get();
    // Add each post to the follower's timeline collection
    postsSnapshot.docs.forEach((doc) {
      if (doc.exists) {
        timelineRef
            .doc(currentUser.id)
            .collection("timelinePosts")
            .doc(doc.id)
            .set(doc.data());
        print("User: ${widget.profileId} followed and "
            "posts added from timeline");
      }
    });
  }

  removeFromTimeline() async {
    // Get the user's timeline to get all the unfollowed
    // user's posts
    QuerySnapshot timelineSnapshot = await timelineRef
        .doc(currentUser.id)
        .collection("timelinePosts")
        .where("ownerId", isEqualTo: widget.profileId)
        .get();
    // Delete the unfollowed user's posts
    timelineSnapshot.docs.forEach((doc) {
      if (doc.exists) {
        doc.reference.delete();
        print("User: ${widget.profileId} unfollowed and "
            "posts removed from timeline");
      }
    });
  }

  buildProfileHeader() {
    //TODO load only post count, follower count and following
    // count using future builder. The rest can be loaded from
    // widget.currentUser
    return FutureBuilder<DocumentSnapshot>(
      future: usersRef.doc(widget.profileId).get(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return circularProgress(context);
        }
        if (snapshot.hasError) {
          print("Something went wrong");
          return Text("Something went wrong");
        }
        if (snapshot.connectionState == ConnectionState.waiting) {
          print("ConnectionState waiting");
          return circularProgress(context);
        }

        LostLoveUser user;
        if (snapshot.connectionState == ConnectionState.done) {
          user = LostLoveUser.fromDocument(
            id: snapshot.data['uid'],
            username: snapshot.data['username'],
            phoneNumber: snapshot.data['phoneNumber'],
            photoUrl: snapshot.data['photoUrl'],
            displayName: snapshot.data['displayName'],
            bio: snapshot.data['bio'],
            lostLove: snapshot.data['lostlove'],
            relationship: snapshot.data['relationship'],
            passing: snapshot.data['passing'],
          );
        }

        return Padding(
          padding: EdgeInsets.fromLTRB(16.0, 3, 16.0, 3),
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  CircleAvatar(
                    radius: 55.0,
                    backgroundColor: Colors.grey,
                    backgroundImage: CachedNetworkImageProvider(user.photoUrl),
                  ),
                  Expanded(
                    flex: 1,
                    child: Column(
                      children: <Widget>[
                        Container(
                          alignment: Alignment.centerLeft,
                          padding: EdgeInsets.fromLTRB(12.0, 20.0, 12.0, 0.0),
                          child: Text(
                            user.username,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0,
                            ),
                          ),
                        ),
                        Container(
                          alignment: Alignment.centerLeft,
                          padding: EdgeInsets.fromLTRB(12.0, 4.0, 12.0, 0.0),
                          child: Text(
                            user.displayName,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Container(
                          alignment: Alignment.centerLeft,
                          padding: EdgeInsets.fromLTRB(12.0, 4.0, 12.0, 0.0),
                          child: Text(
                            user.bio,
                          ),
                        ),
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[buildProfileButton()],
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ],
          ),
        );
      },
    );
  }

  buildProfilePosts() {
    if (isLoading) {
      return circularProgress(context);
    } else if (posts.isEmpty) {
      return Center(
        child: Text(
          'No Posts',
          style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
        ),
      );
    } else if (postOrientation == "grid") {
      List<GridTile> gridTiles = [];
      posts.forEach((post) {
        gridTiles.add(
          GridTile(
            child: PostTile(
              post: post,
            ),
          ),
        );
      });
      return GridView.count(
        crossAxisCount: 3,
        childAspectRatio: 1.0,
        mainAxisSpacing: 1.5,
        crossAxisSpacing: 1.5,
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: gridTiles,
      );
    } else if (postOrientation == "list") {
      return Column(
        children: posts,
      );
    }
  }

  setPostOrientation(String postOrientation) {
    setState(() {
      this.postOrientation = postOrientation;
    });
  }

  buildTogglePostOrientation() {
    return Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          IconButton(
            onPressed: () => setPostOrientation("grid"),
            icon: Icon(Icons.grid_on),
            color: postOrientation == "grid"
                ? Theme.of(context).primaryColor
                : Colors.grey,
          ),
          IconButton(
            onPressed: () => setPostOrientation("list"),
            icon: Icon(Icons.list),
            color: postOrientation == "list"
                ? Theme.of(context).primaryColor
                : Colors.grey,
          ),
        ]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: header(context, titleText: "Profile", executeFunction: true,
          yourFunction: () {
        return showDialog(
            context: context,
            builder: (context) {
              return SimpleDialog(title: Text("Options"), children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(7.0),
                  child: SimpleDialogOption(
                      child: Text("Archived Posts"),
                      onPressed: () {
                        Navigator.pop(context);
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return ArchivedPosts(
                            userId: currentUser.id,
                          );
                        }));
                      }),
                ),
                Padding(
                  padding: const EdgeInsets.all(7.0),
                  child: SimpleDialogOption(
                      child: Text("Log Out"),
                      onPressed: () {
                        Navigator.pop(context);
                        logOutUser();
                      }),
                ),
              ]);
            });
      }),
      body: ListView(
        children: <Widget>[
          buildProfileHeader(),
          Divider(),
          buildTogglePostOrientation(),
          Divider(
            height: 0.0,
          ),
          buildProfilePosts(),
        ],
      ),
    );
  }
}
