import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:lostlove/main.dart';
import 'package:lostlove/models/user.dart';
import 'package:lostlove/pages/edit_profile.dart';
import 'package:lostlove/pages/home.dart';
import 'package:lostlove/widgets/progress.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';

/// This variable stores the instance of the current logged in/registered
/// user.
LostLoveUser userInstance;

class LoginPage extends StatefulWidget {
  static String globalUserId;

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  FirebaseAuth _auth = FirebaseAuth.instance;
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  final _formKey = GlobalKey<FormState>();
  final usernameController = TextEditingController();
  final phoneNumberController = TextEditingController();
  final passwordController = TextEditingController();
  bool isLogin = true, showSpinner = false;

  String phoneNo;
  String smsOTP;
  String verificationId;
  String errorMessage = '';

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    usernameController.dispose();
    phoneNumberController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  /// Save user info in the local memory as shared preferences
  _saveUserInfo({String uid, String phoneNumber, String photoUrl}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setBool('isLoggedIn', true);
      prefs.setBool('isRegistered', true);
      prefs.setString('username', usernameController.text);
      prefs.setString('phoneNumber', phoneNumber);
      prefs.setString('photoUrl', photoUrl);
      prefs.setString('password', passwordController.text);
      prefs.setString('uid', uid);
      prefs.setString('bio', "");
      prefs.setString('lost_love', "");
      prefs.setString('relationship', "");
      prefs.setString('passing', "");
    });
  }

  Future<void> verifyPhone() async {
    this.setState(() {
      showSpinner = !showSpinner;
    });

    final PhoneCodeSent smsOTPSent = (String verId, int forceCodeResend) {
      this.verificationId = verId;
      smsOTPDialog(context).then((value) {
        this.setState(() {
          showSpinner = !showSpinner;
        });
        print('sign in');
      });
    };
    try {
      await _auth.verifyPhoneNumber(
          phoneNumber: this.phoneNo,
          // PHONE NUMBER TO SEND OTP
          codeAutoRetrievalTimeout: (String verId) {
            //Starts the phone number verification process for the given phone number.
            //Either sends an SMS with a 6 digit code to the phone number specified, or sign's the user in and [verificationCompleted] is called.
            this.verificationId = verId;
          },
          codeSent: smsOTPSent,
          // WHEN CODE SENT THEN WE OPEN DIALOG TO ENTER OTP.
          timeout: const Duration(seconds: 20),
          verificationCompleted: (PhoneAuthCredential phoneAuthCredential) {
            print("Phone Auth Credential: $phoneAuthCredential");
          },
          verificationFailed: (FirebaseAuthException exceptio) {
            // TODO display message to user in a toast message
            print('Phone verification failed. Reason: ${exceptio.message}');
            smsOTPDialog(context,
                showErrorMessage: true, errorMessage: exceptio.message);
          });
    } catch (e) {
      handleError(e);
    }
  }

  // code verification dialog
  Future<bool> smsOTPDialog(BuildContext context,
      {bool showErrorMessage = false, String errorMessage = ''}) {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new AlertDialog(
            title: Text(
                showErrorMessage ? "Verification error" : "Enter SMS Code"),
            content: showErrorMessage
                ? Text(errorMessage)
                : Container(
                    height: 85,
                    child: Column(children: [
                      TextField(
                        onChanged: (value) {
                          this.smsOTP = value;
                        },
                      ),
                      (errorMessage != ''
                          ? Text(
                              errorMessage,
                              style: TextStyle(color: Colors.red),
                            )
                          : Container())
                    ]),
                  ),
            contentPadding: EdgeInsets.all(10),
            actions: <Widget>[
              FlatButton(
                child: Text(showErrorMessage ? "Okay" : "Done"),
                onPressed: () {
                  showErrorMessage ? Navigator.pop(context) : signIn();
                  /*_auth.currentUser().then((user) {
                    if (user != null) {
                      Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                          builder: (context) => TheApp(),
                        ),
                      );
                    } else {
                      signIn();
                    }
                  });*/
                },
              )
            ],
          );
        });
  }

  signIn() async {
    try {
      final AuthCredential credential = PhoneAuthProvider.credential(
        verificationId: verificationId,
        smsCode: smsOTP,
      );
      final UserCredential user =
          (await _auth.signInWithCredential(credential));
      final User currentUser = _auth.currentUser;
      assert(user.user.uid == currentUser.uid);

      DocumentSnapshot doc =
          await _firestore.collection('users').doc(currentUser.uid).get();

      if (!doc.exists) {
        // Create a document under the users collection on firebase with the user ID as the document's title
        DocumentReference documentReference =
            _firestore.collection('users').doc(currentUser.uid);
        documentReference.set({
          'uid': currentUser.uid,
          'username': usernameController.text,
          'password': passwordController.text,
          'phoneNumber': phoneNumberController.text,
          'timestamp': DateTime.now(),
          'bio': "",
          'lostlove': "",
          'relationship': "",
          'passing': "",
          'photoUrl': 'url',
          'isAdmin': "",
          'displayName': ""
        });
      } else {
        Fluttertoast.showToast(
            msg: 'User already exists',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
        print('User document already exists');
      }

      createUserInstance(
          id: currentUser.uid,
          username: usernameController.text,
          phoneNumber: phoneNumberController.text);

      // TODO Stop user registration if same username already exists on firebase

      // This will be used to access data specific to this userId
      LoginPage.globalUserId = currentUser.uid;

      // Save user info in shared preferences
      _saveUserInfo(
          uid: currentUser.uid,
          phoneNumber: this.phoneNumberController.text,
          photoUrl: "url");
      print("uid created: ${currentUser.uid}");

      // Once user is created navigate to the home page
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => TheApp(),
        ),
      );
    } catch (e) {
      handleError(e);
    }
  }

  handleError(PlatformException error) {
    print("printe: $error");
    switch (error.code) {
      case 'ERROR_INVALID_VERIFICATION_CODE':
        FocusScope.of(context).requestFocus(new FocusNode());
        setState(() {
          errorMessage = 'Invalid Code';
        });
        Navigator.of(context).pop();
        smsOTPDialog(context).then((value) {
          print('sign in');
        });
        break;
      default:
        setState(() {
          errorMessage = error.message;
        });
        break;
    }
  }

  /// This function creates an instance of user that is described in the user
  /// class.
  void createUserInstance({
    String id,
    String username,
    String phoneNumber,
    String photoUrl,
    String displayName,
    String bio,
  }) {
    // Create an instance of user using the details above
    userInstance = LostLoveUser.fromDocument(
      id: id,
      username: username,
      phoneNumber: phoneNumber,
      photoUrl: photoUrl,
      displayName: displayName,
      bio: bio,
    );
  }

  // for logging in user who's username and password doesn't match with the uid in the shared preferences
  void loginDifferentUser() {
    try {
      _firestore.collection('users').get().then((querySnapshot) {
        for (int x = 0; x <= querySnapshot.size - 1; x++) {
          QueryDocumentSnapshot result = querySnapshot.docs.elementAt(x);

          if (result.data != null) {
            print('Using for loop: ${result.data()}');

            if (result['username'] == usernameController.text) {
              print('Username ${result['username']} exists');

              if (result['password'] == passwordController.text) {
                print(
                    'Password ${result['password']} for ${result['username']} is correct');

                // Update the shared preferences with the userId of the user now logging in
                _saveUserInfo(
                    uid: result['uid'],
                    phoneNumber: result['phoneNumber'],
                    photoUrl: result['photoUrl']);

                createUserInstance(
                    id: result['uid'],
                    username: result['username'],
                    phoneNumber: result['phoneNumber'],
                    photoUrl: result['photoUrl'],
                    displayName: result['displayName'],
                    bio: result['bio']);

                // Login the user
                LoginPage.globalUserId = result['uid'];

                print("Bio is: ${result['bio']}");

                // If user has no bio then send them to complete profile
                if (result['bio'] != "") {
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) => TheApp(),
                    ),
                  );
                } else {
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) => EditProfile(
                        isSigningUp: true,
                        currentUser: userInstance,
                      ),
                    ),
                  );
                }

                break;
              } else {
                Fluttertoast.showToast(
                    msg: 'Incorrect password',
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.CENTER,
                    timeInSecForIosWeb: 1,
                    backgroundColor: Colors.red,
                    textColor: Colors.white,
                    fontSize: 16.0);
                print('Incorrect password');
              }
            } else if (x == querySnapshot.size - 1) {
              Fluttertoast.showToast(
                  msg: 'No such username exists. Kindly register',
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.CENTER,
                  timeInSecForIosWeb: 1,
                  backgroundColor: Colors.red,
                  textColor: Colors.white,
                  fontSize: 16.0);
              print('No such username exists. Kindly register');
            } else {
              continue;
            }
          } else {
            Fluttertoast.showToast(
                msg: 'No users in database. Kindly register',
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.CENTER,
                timeInSecForIosWeb: 1,
                backgroundColor: Colors.red,
                textColor: Colors.white,
                fontSize: 16.0);
            print('No users in database. Kindly register');
          }
        }
      });
    } catch (e) {
      print("Error: $e");
    }
  }

  void loginUser() async {
    this.setState(() {
      showSpinner = !showSpinner;
    });

    // 1st check if the user id is stored in the shared preferences
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // Try reading data from the uid key. If it doesn't exist, return null.
    String userId = prefs.getString('uid') ?? null;
    if (userId == null) {
      print('No user ID in shared preferences');
      // check all documents in firestore to match the username and password
      loginDifferentUser();
    } else {
      // check firestore for the specific document that matches the stored userId (in shared preferences) to match the username and password
      print('User ID exists in shared preferences: $userId');
      try {
        _firestore.collection("users").doc(userId).get().then((result) {
          if (result.data != null) {
            print('Specific user data: ${result.data}');
            if (result['username'] == usernameController.text) {
              print('Username ${result['username']} exists');
              if (result['password'] == passwordController.text) {
                print(
                    'Password ${result['password']} for ${result['username']} is correct');

                // Update the shared preferences with the userId of the user now logging in
                _saveUserInfo(
                    uid: result['uid'],
                    phoneNumber: result['phoneNumber'],
                    photoUrl: result['photoUrl']);

                createUserInstance(
                    id: result['uid'],
                    username: result['username'],
                    phoneNumber: result['phoneNumber'],
                    photoUrl: result['photoUrl'],
                    displayName: result['displayName'],
                    bio: result['bio']);

                // TODO should you remove this variable?
                LoginPage.globalUserId = result['uid'];

                print("Bio is: ${result['bio']}");

                // If user has no bio then send them to complete profile
                if (result['bio'] != "") {
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) => TheApp(),
                    ),
                  );
                } else {
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) => EditProfile(
                        isSigningUp: true,
                        currentUser: userInstance,
                      ),
                    ),
                  );
                }
              } else {
                Fluttertoast.showToast(
                    msg: 'Incorrect password',
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.CENTER,
                    timeInSecForIosWeb: 1,
                    backgroundColor: Colors.red,
                    textColor: Colors.white,
                    fontSize: 16.0);
                print('Incorrect password');
              }
            } else {
              print('Incorrect username. Logging in different user');
              loginDifferentUser();
            }
          } else {
            print("The specified document path doesn't exits");
            loginDifferentUser();
          }
        });
      } catch (e) {
        print("Error: $e");
      }
    }
  }

  // Check if we should display the login interface or the registration interface
  Column columnToDisplay() {
    return isLogin ? loginColumn() : registrationColumn();
  }

  // Registration interface
  Column registrationColumn() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        TextFormField(
          controller: usernameController,
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
            hintText: 'Enter username',
          ),
          validator: (value) {
            if (value.isEmpty) {
              return 'Please enter a username';
            }
            return null;
          },
        ),
        TextFormField(
          controller: passwordController,
          obscureText: true,
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
            hintText: 'Enter a password',
          ),
          validator: (value) {
            if (value.isEmpty) {
              return 'Please enter a password';
            }
            return null;
          },
        ),
        TextFormField(
          controller: phoneNumberController,
          onChanged: (value) {
            this.phoneNo = value;
            print("phone no entered: ${this.phoneNo}");
          },
          keyboardType: TextInputType.phone,
          decoration: InputDecoration(
            hintText: 'Phone number, e.g +254712345678',
          ),
          validator: (value) {
            if (value.isEmpty) {
              return 'Please enter a phone number';
            }
            return null;
          },
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(10.0, 30.0, 10.0, 10.0),
          child: FlatButton(
            color: Colors.blue,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30),
            ),
            child: Text(
              'Register',
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            ),
            onPressed: () {
              //validate will return true if the form is valid, or false if
              // the form is invalid.
              if (_formKey.currentState.validate()) {
                print('Validated');
                verifyPhone();
              } else {
                print('Form not valid');
              }
            },
          ),
        ),
        showSpinner ? circularProgress(context) : Text(""),
        Padding(
          padding: const EdgeInsets.fromLTRB(0.0, 30.0, 0.0, 10.0),
          child: FlatButton(
            onPressed: () {
              setState(() {
                // set to true in order to display the login interface
                isLogin = true;
              });
              print('Is Login is: $isLogin');
            },
            child: Text(
              'Already have an account? Click here to login.',
              style: TextStyle(
                color: Colors.blue,
                fontWeight: FontWeight.bold,
                fontSize: 16,
              ),
            ),
          ),
        ),
      ],
    );
  }

  // Login interface
  Column loginColumn() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        TextFormField(
          controller: usernameController,
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
            hintText: 'Enter username',
          ),
          validator: (value) {
            if (value.isEmpty) {
              return 'Please enter a username';
            }
            return null;
          },
        ),
        TextFormField(
          controller: passwordController,
          obscureText: true,
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
            hintText: 'Enter a password',
          ),
          validator: (value) {
            if (value.isEmpty) {
              return 'Please enter a password';
            }
            return null;
          },
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(10.0, 30.0, 10.0, 10.0),
          child: FlatButton(
            color: Colors.blue,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30),
            ),
            child: Text(
              'Login',
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            ),
            onPressed: () {
              //validate will return true if the form is valid, or false if
              // the form is invalid.
              if (_formKey.currentState.validate()) {
                print('Validated');
                loginUser();
              } else {
                print('Form not valid');
              }
            },
          ),
        ),
        showSpinner ? circularProgress(context) : Text(""),
        Padding(
          padding: const EdgeInsets.fromLTRB(0.0, 30.0, 0.0, 10.0),
          child: FlatButton(
            onPressed: () {
              setState(() {
                // set to false in order to display the registration interface
                isLogin = false;
              });
              print('Is Login is: $isLogin');
              //columnToDisplay();
            },
            child: Text(
              'Don\'t have an account? Click here to register.',
              style: TextStyle(
                color: Colors.blue,
                fontWeight: FontWeight.bold,
                fontSize: 16,
              ),
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 40.0),
                  child: Text(
                    'Lost Love',
                    style: TextStyle(
                      fontFamily: "Signatra",
                      fontSize: 70.0,
                      color: Colors.blue,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Form(
                    key: _formKey,
                    child: columnToDisplay(),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
