import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:lostlove/pages/home.dart';
import 'package:lostlove/widgets/header.dart';
import 'package:lostlove/widgets/post.dart';
import 'package:lostlove/widgets/post_tile.dart';
import 'package:lostlove/widgets/progress.dart';
import 'package:timeago/timeago.dart' as timeago;

class ArchivedPosts extends StatefulWidget {
  final String userId;

  ArchivedPosts({this.userId});

  @override
  ArchivedPostsState createState() => ArchivedPostsState(userId: this.userId);
}

class ArchivedPostsState extends State<ArchivedPosts> {
  final String userId;
  List<Post> posts = [];

  ArchivedPostsState({this.userId});

  buildArchivedPostsGrid(AsyncSnapshot snapshot) {
    if (!snapshot.hasData) {
      return circularProgress(context);
    }

    posts = snapshot.data.docs
        .map((doc) => Post.fromDocument(doc))
        .toList()
        .cast<Post>();

    if (posts.isEmpty) {
      return Center(
        child: Text('No Archived Posts',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0)),
      );
    } else {
      List<GridTile> gridTiles = [];
      posts.forEach((post) {
        gridTiles.add(
          GridTile(
            child: PostTile(
              post: post,
            ),
          ),
        );
      });
      return GridView.count(
        crossAxisCount: 3,
        childAspectRatio: 1.0,
        mainAxisSpacing: 1.5,
        crossAxisSpacing: 1.5,
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: gridTiles,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: postsRef
            .doc(userId)
            .collection("userPosts")
            .orderBy("timestamp", descending: true)
            .where("isArchived", isEqualTo: true)
            .get(),
        builder: (context, snapshot) {
          return Center(
            child: Scaffold(
              appBar: header(context, titleText: "Archived Posts"),
              body: buildArchivedPostsGrid(snapshot),
            ),
          );
        });
  }
}
