import 'package:flutter/material.dart';
import 'package:lostlove/pages/home.dart';
import 'package:lostlove/widgets/header.dart';
import 'package:lostlove/widgets/post.dart';
import 'package:lostlove/widgets/progress.dart';

class PostScreen extends StatelessWidget {
  final String userId, postId;

  PostScreen({this.userId, this.postId});

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: postsRef.doc(userId).collection("userPosts").doc(postId).get(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return circularProgress(context);
          }
          Post post = Post.fromDocument(snapshot.data);
          return Center(
            child: Scaffold(
              appBar: header(context, titleText: post.description),
              body: ListView(
                children: <Widget>[
                  Container(
                    child: post,
                  )
                ],
              ),
            ),
          );
        });
  }
}
