import 'package:cloud_firestore/cloud_firestore.dart';

/// User class to create a user object to use to hold user's info.
class LostLoveUser {
  final String id;
  final String username;
  final String phoneNumber;
  final String photoUrl;
  final String displayName;
  final String bio;
  // A Lost Love is the loved one who a user lost
  final String lostLove;
  // The type of relationship between a user and his/her lost love
  final String relationship;
  // How the lost loved one died
  final String passing;

  LostLoveUser({
    this.id,
    this.username,
    this.phoneNumber,
    this.photoUrl,
    this.displayName,
    this.bio,
    this.lostLove,
    this.relationship,
    this.passing,
  });

  // Create a function that returns an instance of User
  /// A function that returns an instance of user from the document
  /// created for a user when a user registers.
  factory LostLoveUser.fromDocument({
    String id,
    String username,
    String phoneNumber,
    String photoUrl,
    String displayName,
    String bio,
    String lostLove,
    String relationship,
    String passing,
  }) {
    return LostLoveUser(
      id: id,
      username: username,
      phoneNumber: phoneNumber,
      photoUrl: photoUrl,
      displayName: displayName,
      bio: bio,
      lostLove: lostLove,
      relationship: relationship,
      passing: passing,
    );
  }
}
