import 'package:flutter/material.dart';

/// A customer header that accepts the arguments [context] and the named
/// arguments [isAppTitle], [titleText], [executeFunction] and [yourFunction].
AppBar header(
  context, {
  bool isAppTitle = false,
  String titleText,
  bool executeFunction = false,
  Function yourFunction,
}) {
  return AppBar(
    title: Text(
      isAppTitle ? "Lost Love" : titleText,
      style: TextStyle(
        color: Colors.black,
        fontFamily: isAppTitle ? "Signatra" : "",
        fontSize: isAppTitle ? 35.0 : 18.0,
      ),
      overflow: TextOverflow.ellipsis,
    ),
    centerTitle: isAppTitle ? true : false,
    backgroundColor: Colors.white,
    actions: <Widget>[
      executeFunction
          ? FlatButton(
              child: titleText == "Edit profile"
                  ? Icon(Icons.done, size: 30.0, color: Colors.green)
                  : Icon(Icons.more_vert, color: Colors.grey),
              onPressed: yourFunction,
            )
          : SizedBox(
              width: 1,
            )
    ],
  );
}
