import 'package:flutter/material.dart';

class Accordion extends StatefulWidget {
  final String title;
  final Widget children;
  final bool showContent;

  Accordion({this.title, this.children, this.showContent = false});

  @override
  _AccordionState createState() => _AccordionState(
        title: this.title,
        children: this.children,
        showContent: this.showContent,
      );
}

class _AccordionState extends State<Accordion> {
  final String title;
  final Widget children;
  bool showContent;

  _AccordionState({this.title, this.children, this.showContent = false});

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(10),
      child: Column(children: [
        ListTile(
          title: Text(widget.title),
          trailing: IconButton(
            icon:
                Icon(showContent ? Icons.arrow_drop_up : Icons.arrow_drop_down),
            onPressed: () {
              setState(() {
                showContent = !showContent;
              });
            },
          ),
        ),
        showContent
            ? Container(
                padding: EdgeInsets.symmetric(vertical: 12, horizontal: 12),
                child: widget.children,
              )
            : Container()
      ]),
    );
  }
}
