import 'package:flutter/material.dart';

// circular spinner to show loading progress
Container circularProgress(context) {
  return Container(
    // position the spinner to the middel of the page both horizontally and vertically
    alignment: Alignment.center,
    padding: EdgeInsets.only(top: 10.0),
    child: CircularProgressIndicator(
      valueColor: AlwaysStoppedAnimation(Theme.of(context).primaryColor),
    ),
  );
}

// linear progress bar to show loading progress
Container linearProgress(context) {
  return Container(
    padding: EdgeInsets.only(bottom: 10.0),
    child: LinearProgressIndicator(
      valueColor: AlwaysStoppedAnimation(Theme.of(context).primaryColor),
    ),
  );
}
