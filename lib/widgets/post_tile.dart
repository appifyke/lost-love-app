import 'package:flutter/material.dart';
import 'package:lostlove/pages/post_screen.dart';
import 'package:lostlove/widgets/custom_image.dart';
import 'package:lostlove/widgets/post.dart';

class PostTile extends StatelessWidget {
  final Post post;

  PostTile({this.post});

  showPost(context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => PostScreen(
          postId: post.postId,
          userId: post.ownerId,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => showPost(context),
      child: post.mediaUrl.isEmpty
          ? Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.remove_red_eye,
                  color: Colors.blue,
                ),
                Text(
                  "Click to view",
                  style: TextStyle(
                      fontFamily: 'Open Sans',
                      fontSize: 10.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.blue),
                ),
              ],
            )
          : cachedNetworkImage(post.mediaUrl),
    );
  }
}
