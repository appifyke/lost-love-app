import 'package:flutter/material.dart';

/// A custom dialog for the user to confirm important actions.
/// This dialog receives a [function] as an argument. The function executes
/// when the user presses the Confirm button.
SimpleDialog dialog({Function function, BuildContext context}) {
  return SimpleDialog(
    title: Text("Are you sure?"),
    children: <Widget>[
      Padding(
        padding: const EdgeInsets.all(7),
        child: SimpleDialogOption(
          child: Text("Yes"),
          onPressed: () {
            Navigator.pop(context);
            function();
          },
        ),
      ),
      Padding(
        padding: const EdgeInsets.all(7),
        child: SimpleDialogOption(
          child: Text("Cancel"),
          onPressed: () => Navigator.pop(context),
        ),
      )
    ],
  );
}
