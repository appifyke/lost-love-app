import 'dart:async';

import 'package:animator/animator.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:lostlove/models/user.dart';
import 'package:lostlove/pages/activity_feed.dart';
import 'package:lostlove/pages/comments.dart';
import 'package:lostlove/pages/home.dart';
import 'package:lostlove/pages/timeline.dart';
import 'package:lostlove/widgets/dialog.dart';
import 'package:lostlove/widgets/progress.dart';
import 'package:lostlove/widgets/custom_image.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:readmore/readmore.dart';

class Post extends StatefulWidget {
  final String postId;
  final String ownerId;
  final String username;
  final String location;
  final String description;
  final String mediaUrl;
  final dynamic likes;
  final Timestamp timestamp;

  Post({
    this.postId,
    this.ownerId,
    this.username,
    this.location,
    this.description,
    this.mediaUrl,
    this.likes,
    this.timestamp,
  });

  //creating a post instance from a firestore document
  factory Post.fromDocument(DocumentSnapshot doc) {
    return Post(
      postId: doc['postId'],
      ownerId: doc['ownerId'],
      username: doc['username'],
      location: doc['location'],
      description: doc['description'],
      mediaUrl: doc['mediaUrl'],
      likes: doc['likes'],
      timestamp: doc['timestamp'],
    );
  }

  int getLikeCount(likes) {
    // if there are no likes, return 0
    if (likes == null) {
      return 0;
    }
    int count = 0;
    // if the key is set to true then add a like
    likes.values.forEach((val) {
      if (val == true) {
        count += 1;
      }
    });
    return count;
  }

  @override
  _PostState createState() => _PostState(
      postId: this.postId,
      ownerId: this.ownerId,
      username: this.username,
      location: this.location,
      caption: this.description,
      mediaUrl: this.mediaUrl,
      likes: this.likes,
      likeCount: getLikeCount(this.likes),
      timestamp: this.timestamp);
}

class _PostState extends State<Post> {
  final String currentUserId = currentUser?.id;
  final String postId;
  final String ownerId;
  final String username;
  final String location;
  final String caption;
  final String mediaUrl;
  final Timestamp timestamp;
  Map likes;
  int likeCount;
  bool isLiked;
  bool showHeart = false;

  _PostState({
    this.postId,
    this.ownerId,
    this.username,
    this.location,
    this.caption,
    this.mediaUrl,
    this.likes,
    this.likeCount,
    this.timestamp,
  });

  buildPostHeader() {
    return FutureBuilder(
      future: usersRef.doc(ownerId).get(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return circularProgress(context);
        }
        LostLoveUser user = LostLoveUser.fromDocument(
          id: snapshot.data['uid'],
          username: snapshot.data['username'],
          phoneNumber: snapshot.data['phoneNumber'],
          photoUrl: snapshot.data['photoUrl'],
          displayName: snapshot.data['displayName'],
          bio: snapshot.data['bio'],
        );
        bool isPostOwner = currentUserId == ownerId;
        return ListTile(
          leading: CircleAvatar(
            backgroundImage: CachedNetworkImageProvider(user.photoUrl),
            backgroundColor: Colors.grey,
          ),
          title: GestureDetector(
            onTap: () => showProfile(
              context,
              profileId: user.id,
            ),
            child: Text(
              user.username,
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          subtitle: Text("${timeago.format(timestamp.toDate())} $location"),
          trailing: isPostOwner
              ? IconButton(
                  onPressed: () => handlePost(context),
                  icon: Icon(Icons.more_vert),
                )
              : Text(""),
        );
      },
    );
  }

  handlePost(BuildContext parentContext) {
    return showDialog(
        context: parentContext,
        builder: (context) {
          return SimpleDialog(
            title: Text("Options"),
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(7.0),
                child: SimpleDialogOption(
                  onPressed: () {
                    Navigator.pop(context);
                    editPost();
                  },
                  child: Text(
                    "Edit post",
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(7.0),
                child: SimpleDialogOption(
                  child: Text("Archive post"),
                  onPressed: () {
                    Navigator.pop(context);
                    archivePost();
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(7.0),
                child: SimpleDialogOption(
                  onPressed: () {
                    Navigator.pop(context);
                    deletePost(context);
                  },
                  child: Text(
                    "Delete post",
                    style: TextStyle(color: Colors.red),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(7.0),
                child: SimpleDialogOption(
                  onPressed: () => Navigator.pop(context),
                  child: Text(
                    "Cancel",
                  ),
                ),
              ),
            ],
          );
        });
  }

  //TODO enable editing a post
  editPost() {
    // edit post
  }

  archivePost() {
    postsRef
        .doc(ownerId)
        .collection("userPosts")
        .doc(postId)
        .update({"isArchived": true});
    // TODO return to updated profile page after archiving a post. Updated profile page shouldn't contain the archive post. Archived posts should appear on a separate page
  }

  // To delete a post, ownerId and currentUserId must be
  // equal to be used interchangeably
  deletePost(BuildContext parentContext) async {
    return showDialog(
        context: parentContext,
        builder: (context) {
          return dialog(
              context: context,
              function: () async {
                // Delete the post itself
                postsRef
                    .doc(ownerId)
                    .collection("userPosts")
                    .doc(postId)
                    .get()
                    .then((doc) {
                  if (doc.exists) {
                    doc.reference.delete();
                  }
                });
                // Delete uploaded image for the post
                storageRef.child("post_$postId.jpg").delete();

                // Delete all activity feed notifications
                QuerySnapshot activityFeedSnapshot = await activityFeedRef
                    .doc(ownerId)
                    .collection("feedItems")
                    .where("postId", isEqualTo: postId)
                    .get();

                activityFeedSnapshot.docs.forEach((doc) {
                  if (doc.exists) {
                    doc.reference.delete();
                  }
                });

                // Delete all comments
                QuerySnapshot commentsSnapshot =
                    await commentsRef.doc(postId).collection("comments").get();

                commentsSnapshot.docs.forEach((doc) {
                  if (doc.exists) {
                    doc.reference.delete();
                  }
                });

                Fluttertoast.showToast(
                    msg: 'Post deleted',
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.CENTER,
                    timeInSecForIosWeb: 1,
                    backgroundColor: Colors.red,
                    textColor: Colors.white,
                    fontSize: 16.0);
                print('Post deleted');
                //TODO navigate back to the profile page that doesn't contain the deleted post
                onDeletePost(postId);
              });
        });
  }

  /// When a post is deleted, remove it from the
  /// timeline of each follower of the post owner
  onDeletePost(String pid) async {
    // Get the current user's followers from the followersRef
    QuerySnapshot followersSnapshot = await followersRef
        .doc(currentUser.id)
        .collection("userFollowers")
        .get();
    // Remove the post that's being deleted from each followers'
    // timelines
    followersSnapshot.docs.forEach((doc) {
      // Delete post
      timelineRef
          .doc(doc.id)
          .collection("timelinePosts")
          .doc(pid)
          .get()
          .then((post) {
        if (post.exists) {
          //print("The post: $post");
          post.reference.delete();
        }
      });
    });
  }

  handleLikePost() {
    bool _isLiked = likes[currentUserId] == true;
    if (_isLiked) {
      postsRef
          .doc(ownerId)
          .collection('userPosts')
          .doc(postId)
          .update({'likes.$currentUserId': false});
      setState(() {
        likeCount -= 1;
        isLiked = false;
        likes[currentUserId] = false;
      });
      removeLikeFromActivityFeed();
    } else if (!_isLiked) {
      postsRef.doc(ownerId).collection('userPosts').doc(postId).update({
        'likes.$currentUserId': true,
      });
      addLikeToActivityFeed();
      setState(() {
        likeCount += 1;
        isLiked = true;
        likes[currentUserId] = true;
        showHeart = true;
      });
      Timer(Duration(milliseconds: 500), () {
        setState(() {
          showHeart = false;
        });
      });
    }
  }

  addLikeToActivityFeed() {
    // add notification to activity feed only if comments/likes
    // are made by other users to avoid getting notifications
    // of the owner of the post liking or commenting on his/her
    // own posts
    bool isNotPostOwner = currentUserId != ownerId;
    if (isNotPostOwner) {
      activityFeedRef.doc(ownerId).collection("feedItems").doc(postId).set({
        "type": "like",
        "username": currentUser.username,
        "userId": currentUser.id,
        "userProfileImg": currentUser.photoUrl,
        "postId": postId,
        "mediaUrl": mediaUrl,
        "timestamp": DateTime.now(),
      });
      onCreateActivityFeedItem();
    }
  }

  onCreateActivityFeedItem() {
    // When a user likes posts, comments on posts or follows another user, an
    // activity feed item is created and we want to use that to also add or
    // record a notification in a notification collection with the following
    // structure: collection > userId > notificationItems > notificationId >
    // notification data including the notification receiver's userId, the
    // notification type (like, comment or follow) and the postId in the case
    // the notification type is a like or comment so that we can display a
    // thumbnail image of the post with the notification. If the notification
    // is of type follow, we will display a follow button with the notification.
    notificationRef.doc(ownerId).collection("notificationItems").doc().set({
      "type": "like",
      "receiver": ownerId,
      "sender": currentUser.id,
      "postId": postId,
      "timestamp": DateTime.now(),
    });
  }

  removeLikeFromActivityFeed() {
    bool isNotPostOwner = currentUserId != ownerId;
    if (isNotPostOwner) {
      activityFeedRef
          .doc(ownerId)
          .collection("feedItems")
          .doc(postId)
          .get()
          .then((doc) {
        if (doc.exists) {
          doc.reference.delete();
        }
      });
    }
  }

  buildPostBody() {
    return GestureDetector(
      onDoubleTap: handleLikePost,
      child: mediaUrl.isNotEmpty
          ? Stack(
              alignment: Alignment.center,
              children: <Widget>[
                ClipRRect(
                  child: cachedNetworkImage(mediaUrl),
                  borderRadius: BorderRadius.circular(30.0),
                ),
                showHeart
                    ? Animator(
                        duration: Duration(milliseconds: 300),
                        tween: Tween(begin: 0.8, end: 1.4),
                        curve: Curves.elasticOut,
                        cycles: 0,
                        builder: (context, anim, child) => Transform.scale(
                          scale: anim.value,
                          child: Icon(
                            Icons.favorite,
                            size: 80.0,
                            color: Colors.red,
                          ),
                        ),
                      )
                    : Text(""),
                // showHeart
              ],
            )
          : Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Flexible(
                  child: Container(
                    child: ReadMoreText(
                      "$caption",
                      trimLines: 7,
                      colorClickableText: Colors.blue,
                      trimMode: TrimMode.Line,
                      trimCollapsedText: 'Read more',
                      trimExpandedText: 'Read less',
                      moreStyle: TextStyle(
                        fontFamily: "Open Sans",
                        fontWeight: FontWeight.w600,
                      ),
                      lessStyle: TextStyle(
                        fontFamily: "Open Sans",
                        fontWeight: FontWeight.w600,
                      ),
                      style: TextStyle(
                          fontFamily: "Open Sans",
                          fontWeight: FontWeight.w600,
                          color: Colors.black),
                    ),
                  ),
                ),
              ],
            ),
    );
  }

  buildPostFooter() {
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 40.0, left: 20.0),
            ),
            GestureDetector(
              onTap: handleLikePost,
              child: Icon(
                isLiked ? Icons.favorite : Icons.favorite_border,
                size: 28.0,
                color: Colors.pink,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(right: 20.0),
            ),
            GestureDetector(
              onTap: () => showComments(
                context,
                postId: postId,
                ownerId: ownerId,
                mediaUrl: mediaUrl,
              ),
              child: Icon(
                Icons.chat,
                size: 28.0,
                color: Colors.blue[900],
              ),
            ),
          ],
        ),
        Row(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 20.0),
              child: Text(
                "$likeCount likes",
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                ),
              ),
            )
          ],
        ),
        mediaUrl.isNotEmpty
            ? Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(left: 20.0),
                    child: Text(
                      "$caption",
                    ),
                  ),
                ],
              )
            : Text(""),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    isLiked = (likes[currentUserId] == true);
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        buildPostHeader(),
        Padding(
          child: buildPostBody(),
          padding: EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 12.0),
        ),
        buildPostFooter(),
      ],
    );
  }
}

showComments(
  BuildContext context, {
  String postId,
  String ownerId,
  String mediaUrl,
}) {
  Navigator.push(context, MaterialPageRoute(builder: (context) {
    return Comments(
      postId: postId,
      postOwnerId: ownerId,
      postMediaUrl: mediaUrl,
    );
  }));
}
