import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:lostlove/models/user.dart';
import 'package:lostlove/pages/home.dart';
import 'package:lostlove/pages/login_page.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // TODO To remove below commented code
  // FirebaseFirestore.instance.settings(timestampsInSnapshotsEnabled: true).then(
  //     (_) {
  //   print("Timestamps enabled in snapshots\n");
  // }, onError: (_) {
  //   print("Error enabling timestamps in snapshots\n");
  // });

  await Firebase.initializeApp();

  runApp(TheApp());
}

class TheApp extends StatelessWidget {
  /*@override
  _TheAppState createState() => _TheAppState();*/

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Lost Love',
      debugShowCheckedModeBanner: false,
      routes: <String, WidgetBuilder>{
        '/homepage': (BuildContext context) => TheApp(),
        '/loginpage': (BuildContext context) => LoginPage(),
      },
      // create a theme to use throughout the app. You can use the material io website to help you choose a theme color
      theme: ThemeData(
        // setting the primary and accent colors for the theme
        primarySwatch: Colors.blue,
        accentColor: Colors.blue,
      ),
      home: Home(),
    );
  }
}

/*class _TheAppState extends State<TheApp> {
  @override
  void initState() {
    // Detects when user signed in or signed out
    FirebaseAuth.instance.currentUser().then((currentUser) {
      if (currentUser == null) {
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => LoginPage(),
          ),
        );
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Lost Love',
      debugShowCheckedModeBanner: false,
      routes: <String, WidgetBuilder>{
        '/homepage': (BuildContext context) => TheApp(),
        '/loginpage': (BuildContext context) => LoginPage(),
      },
      // create a theme to use throughout the app. You can use the material io website to help you choose a theme color
      theme: ThemeData(
        // setting the primary and accent colors for the theme
        primarySwatch: Colors.blue,
        accentColor: Colors.white,
      ),
      home: Home(),
    );
  }
}*/
